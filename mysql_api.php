<?php
	
	class MySQL	{
		
		protected static $dbs;
		
		function __construct(array $mysql_info=array(), int $db_num=null)	{ //connect to a new db or select one that already exists
			if(count($mysql_info) > 0)	{
				$conn_num = $this->connect($mysql_info);
				$this->select_connection($conn_num)
			}
			if(isset($db_num) == true)	{
				$this->select_connection($db_num);
			}
		}
		
		function connect(array $info)	{ //add a new db connection;
			self::$dbs[] = new mysqli($info['h'], $info['u'], $info['p'], $info['d']) //hostname, username, password, database;
			return count($this->db)-1;
		}
		
		function select_connection(int $num)	{ // switch databases connections
			if(array_key_exists($num, self::$dbs) == true)	{
				$this->db =& self::$dbs[$num];
				$this->db_num = $num;
			}
		}
		
		public function query($sql)	{ // this uses printf sytnax have fun
			if(func_num_args() > 1)	{ //need to build query
				$args = func_get_args();//get arguments
				unset($args[0]); //this is the query its self
				$args = $this->_filter($args); //filter things
				$sql = call_user_func_array('sprintf', array_merge(array($sql), $args));
			}
			
			$this->db->query($sql);
		}
		
		protected function _filter(array $inputs)	{
			foreach($inputs as $dex => $dat)	{
				$inputs[$dex] = $this->db->escape_string($dat);
			}
			return $inputs;
		}
	}
?>
